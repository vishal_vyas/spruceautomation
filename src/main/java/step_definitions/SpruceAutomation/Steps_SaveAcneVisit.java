package step_definitions.SpruceAutomation;

import androidPages.VisitAlertPopupPage;
import androidPages.YourSymptomsPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps_SaveAcneVisit extends Steps_Base {

	VisitAlertPopupPage visitAlertPopupPage = new VisitAlertPopupPage();
	YourSymptomsPage yourSymptomsPage = new YourSymptomsPage();
	
	@When("^I tap on Save Draft button$")
	public void i_tap_on_Save_Draft_button() {
		abortScenarioIfFalse(visitAlertPopupPage, visitAlertPopupPage.clickOnContinueSaveVisitButton());
	}

	@Then("^Verify Continue Visit popup is shown$")
	public void verify_Continue_Visit_popup_is_shown() {
		abortScenarioIfFalse(visitAlertPopupPage, visitAlertPopupPage.verifyVisitAlertPopupPage());
	}

	@When("^I tap on Continue Visit button$")
	public void i_tap_on_Continue_Visit_button() {
		abortScenarioIfFalse(visitAlertPopupPage, visitAlertPopupPage.clickOnContinueSaveVisitButton());
	}

	@Then("^Verify \"([^\"]*)\" symptom is shown selected$")
	public void verify_symptom_is_shown_selected(String arg1) {
		abortScenarioIfFalse(yourSymptomsPage, yourSymptomsPage.verifySymptomSelected(arg1));
	}
}
