package androidPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import modules.FrameworkConstants;

public class StartVisitPage extends SpruceAppBase {

	private By lblHeader = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/toolbar']/android.widget.TextView");

	private By btnClose = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/toolbar']/android.widget.ImageButton");

	private By lstVisitOption = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/content']/android.widget.Button"); 
	
	private final String PAGE_DISPLAY_NAME = "Start a Visit";
	
	public StartVisitPage() {
		// if first add is successful, then add the remaining ones. Next time if first one is added, assume that rest would already be added.
		if (addElementDetails(lstVisitOption.toString(), "Visit Options", PAGE_DISPLAY_NAME))
		{
			addElementDetails(btnClose.toString(), "Close button", PAGE_DISPLAY_NAME);
			addElementDetails(lblHeader.toString(), "Start Visit Header", PAGE_DISPLAY_NAME);
		}
	}
	
	public boolean verifyStartVisitPage(String header){
		addDelay(FrameworkConstants.WAIT_2, "Make sure Start Visit page is loaded.");
		WebElement eleHeader = isElementVisible(lblHeader, "lblHeader");
		if (eleHeader!=null) {
			String headerText = getElementText(lblHeader, eleHeader, "lblHeader");
			if (headerText != null && headerText.contains(header)) {
				logger.info("Start Visit header text is matched...");
				return true;
			} else {
				setLastErrorLocator(lblHeader);
				setLastErrorMessage("Failed to match " + header + " header.");
				setLastLogicalErrorMessage("Failed to match " + header + " header!");
				logger.error("TEST STEP ERROR - Failed to match " + header + " header...");
				return false;
			}
		}  else {
			setLastLogicalErrorMessage("Failed to display Start Visit header!");
			return false;
		}
	}
	
	public boolean verifyVisitOption() {
		List<WebElement> lstComposeMessage = getVisibleElementList(lstVisitOption, "Visit Options");
		if (lstComposeMessage != null && lstComposeMessage.size()>0) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to display List of Visit options!");
			return false;
		}
	}
	
	public boolean clickOnVisitOption(String visitOption) {
		boolean isVisitOptionFound = false;
		WebElement eleVisitOption = isElementVisible(lstVisitOption, "Visit Options");
		if (eleVisitOption!=null) {
			String option = getElementText(lstVisitOption, eleVisitOption, "Visit Options");
			if (option != null && option.contains(visitOption)) {
				if(!clickOnElement(lstVisitOption, eleVisitOption, "Visit Options")) {
					setLastLogicalErrorMessage("Failed to click on " + visitOption + "!");
				}
				isVisitOptionFound = true;
			} else {
				setLastErrorLocator(lstVisitOption);
				setLastErrorMessage("Failed to display " + visitOption + " visit option!");
				setLastLogicalErrorMessage("Failed to display " + visitOption + " visit option!");
				logger.error("TEST STEP ERROR - Failed to display " + visitOption + " visit option!");
			}
		}  else {
			setLastLogicalErrorMessage("Failed to display Visit options!");
		}
		return isVisitOptionFound;
	}
}
