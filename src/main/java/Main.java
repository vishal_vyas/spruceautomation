import java.io.IOException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.runner.JUnitCore;
import modules.LodestoneAutomation;


public class Main {
	private static Logger logger = Logger.getLogger("TestRunOutput");
	
	// CUSTOMER GUID
	// REDDIT DB
	private static String customerId = "0d45325f-5a97-11e8-b493-0a7be966bb00";
	
	// REDDIT DEV DB
	//private static String customerId = "5368d7ef-63c7-11e8-b493-0a7be966bb00";
	
	public static void main(String args[]) throws IOException {	
		
		String action = "";
		int runId = 0;
		
		// READ ARGUMENTS PASSED TO THE JAR
		if (args.length > 0) {
			if (args[0] != null) {
				action = args[0];
			}
		}
		if (args.length > 1) {
			if (args[1] != null) {
				runId = Integer.parseInt(args[1]);
			}
		}
		if (args.length > 2) {
			if (args[2] != null) {
				customerId = args[2];
			}
		}
		
		// START RUN
		if(action.toLowerCase().equals("-run")) {
			boolean isFrameworkInitialized = LodestoneAutomation.initializeFramework(customerId, runId, "configuration/config.yml", Level.INFO, Level.INFO, true, false);
			
			if(isFrameworkInitialized) {
				logger.info("**************************************************");
				logger.info("STARTING SPRUCE AUTOMATION FOR RUN ID =" + runId);
				logger.info("**************************************************");
				
				try {
					// ACTUAL TEST CASE STARTS FOR CUCUMBER
					JUnitCore.runClasses(RunCukesTest.class);
				} catch(Exception e) {
					// CUCUMBER FAILURE
					logger.error("Error in Cucumber run => "+ e.getLocalizedMessage());
				}
				logger.info("CUCUMBER PROPERTIES = " + System.getProperty("cucumber.options"));
			
				LodestoneAutomation.prepareRunOutput();
				LodestoneAutomation.shutdownFramework();
			}
		}
	}
}