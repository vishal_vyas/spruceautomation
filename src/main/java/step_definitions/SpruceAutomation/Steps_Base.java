package step_definitions.SpruceAutomation;

import java.awt.image.BufferedImage;
import java.util.Random;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import modules.FrameworkConstants;
import modules.LodestoneAutomation;
import modules.LodestoneAutomationTestDataHandler;
import modules.LodestoneBase;
import modules.errorhandlers.LodestoneErrorHandler;
import modules.exceptions.CustomException;

public class Steps_Base {
	
	LodestoneErrorHandler lodestoneErrorHandler = LodestoneAutomation.getLodestoneErrorHandler();
	public Logger logger = Logger.getLogger(FrameworkConstants.TESTRUN_LOGGER);
	public String defaultTarget = "TARGET_1"; 
	
	private void abortScenario(LodestoneBase page, Exception ex, By locator, String errorMessage, String logicalErrorMessage) {
		LodestoneAutomation.capturePageSource(page.toString());
		lodestoneErrorHandler.addErrorMessage(ex, locator, errorMessage, logicalErrorMessage);
		page.logger.error(logicalErrorMessage);
		page.addDelay(FrameworkConstants.WAIT_10, "Record video after test scenario fails.");
		Assert.fail();
	}
	
	public void abortScenarioIfFalse(LodestoneBase page, Boolean conditionMet) {
		if (!conditionMet) {
			By failedElementLocator = page.getLastErrorLocator();
			String errorMessage = page.getLastErrorMessage();
			String logicalErrorMessage = page.getLastLogicalErrorMessage();
			if(logicalErrorMessage == null)
				logicalErrorMessage = "Not Deterministic";
			if(page.isAppCrashed()) {
				abortScenario(page, new CustomException("Application Crashed"), failedElementLocator, "Application Crashed", "Application Crashed");
			} else {
				if (failedElementLocator == null)
					abortScenario(page, null, failedElementLocator, errorMessage, logicalErrorMessage);
				else
					abortScenario(page, new org.openqa.selenium.NoSuchElementException(errorMessage), failedElementLocator, errorMessage, logicalErrorMessage);	
			}
			
		}
	}

	public void abortScenarioIfTrue(LodestoneBase page, Boolean conditionMet) {
		if (conditionMet) {
			By failedElementLocator = page.getLastErrorLocator();
			String errorMessage = page.getLastErrorMessage();
			String logicalErrorMessage = page.getLastLogicalErrorMessage();
			if(logicalErrorMessage == null)
				logicalErrorMessage = "Not Deterministic";
			if(page.isAppCrashed()) {
				abortScenario(page, new CustomException("Application Crashed"), failedElementLocator, "Application Crashed", "Application Crashed");
			} else {
				if (failedElementLocator == null)
					abortScenario(page, null, failedElementLocator, errorMessage, logicalErrorMessage);
				else
					abortScenario(page, new org.openqa.selenium.NoSuchElementException(errorMessage), failedElementLocator, errorMessage, logicalErrorMessage);
			}
		}
	}

	public String getUniqueTestData(String testData) {
		Random rand = new Random();
		int number = rand.nextInt(100000);
		testData = testData + "-" + number;
		return testData;
	}
	
	public String getUniqueTestDataForLink(String testData) {
		Random rand = new Random();
		int number = rand.nextInt(100000);
		testData = testData + "/" + number;
		return testData;
	}
	
	public void addTestDataInCollection(String key, String testData) {
		LodestoneAutomationTestDataHandler.addTestDataInCollection(key, testData);
	}
	
	public void addTestImageDataInCollection(String key, BufferedImage imageData) {
		LodestoneAutomationTestDataHandler.addTestDataInCollection(key, imageData);
	}
	
	public String getValueFromTestDataCollection(String key) {
		return LodestoneAutomationTestDataHandler.getValueFromTestDataCollection(key);
	}
	
	public BufferedImage getImageTestDataFromCollection(String key) {
		return LodestoneAutomationTestDataHandler.getImageTestDataFromCollection(key);
	}
}
