package step_definitions;

import java.io.IOException;
import org.apache.log4j.Logger;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import modules.FrameworkConstants;
import modules.LodestoneAutomation;

public class Hooks {
	
	private Logger logger = Logger.getLogger(FrameworkConstants.TESTRUN_LOGGER);

	@Before
	public void beforeScenario(Scenario scenario) {
		logger.info("");
		logger.info("+++ BEFORE SCENARIO - ENTER : " + scenario.getName() + " +++");
		LodestoneAutomation.initScenario(scenario);
		
		// DIPAM TODO : What is this?
		// TEST CASE PARAMETERS ARE SET FROM CONFIG
		/*
		String testDataFile = LodestoneAutomation.testDataFile;
		if(testDataFile!=null && !testDataFile.equals("")) {
			Utility.setConfigFile(testDataFile);
		}
		*/
		logger.info("+++ BEFORE SCENARIO - EXIT : " + scenario.getName() + " +++");
		logger.info("");
	}

	@After
	/**
	 * Embed a screenshot in test report if test is marked as failed
	 */
	public void afterScenario(Scenario scenario) throws IOException {
		logger.info("");
		logger.info("+++ AFTER SCENARIO - ENTER  => " + scenario.getName() + " || Status => " + scenario.getStatus() + " +++");
		// PREPARE CUCUMBER OUTPUT WITH SNAPSHOT AND VIDEO
		LodestoneAutomation.prepareCucumberOutput(scenario);
		logger.info("+++ AFTER SCENARIO - EXIT  => " + scenario.getName() + " +++");
		logger.info("");
	}
}
