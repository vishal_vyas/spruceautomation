package androidPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import modules.configurations.AndroidBase;

public class SpruceAppBase extends AndroidBase {

//	@FindBy(id ="android:id/alertTitle")
//	private WebElement lblAppCrashTitle;
	private By lblAppCrashTitleLocator = By.id("android:id/alertTitle");
	
	private final String PAGE_DISPLAY_NAME = "Spruce Application Base";
	
	public SpruceAppBase() {
		addElementDetails(lblAppCrashTitleLocator.toString(), "Application Crashed...", PAGE_DISPLAY_NAME);
	}
	
	public boolean isAppCrashed() {
		boolean isAppCrashed =false;
		WebElement lblAppCrashTitle = isElementVisible(lblAppCrashTitleLocator, "lblAppCrashTitle");
		if(lblAppCrashTitle != null) {
			String lblAppCrashTitleText=getElementText(lblAppCrashTitleLocator, lblAppCrashTitle, "lblAppCrashTitle");
			isAppCrashed=lblAppCrashTitleText.contains("Spruce");
		}
		return isAppCrashed;
	}
}
