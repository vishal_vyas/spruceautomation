package androidPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import modules.FrameworkConstants;

public class VisitPage extends SpruceAppBase {

	private By lblHeader = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/toolbar']/android.widget.TextView");

	private By btnClose = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/toolbar']/android.widget.ImageButton");

	private By btnBeginVisit = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/bottomBarButton']");
	
	private final String PAGE_DISPLAY_NAME = "Visit";
	
	public VisitPage() {
		// if first add is successful, then add the remaining ones. Next time if first one is added, assume that rest would already be added.
		if (addElementDetails(btnBeginVisit.toString(), "Begin Visit Button", PAGE_DISPLAY_NAME))
		{
			addElementDetails(btnClose.toString(), "Close button", PAGE_DISPLAY_NAME);
			addElementDetails(lblHeader.toString(), "Visit Header", PAGE_DISPLAY_NAME);
		}
	}
	
	public boolean verifyVisitPage(String header){
		addDelay(FrameworkConstants.WAIT_2, "Make sure Visit page is loaded.");
		WebElement eleHeader = isElementVisible(lblHeader, "lblHeader");
		if (eleHeader!=null) {
			String headerText = getElementText(lblHeader, eleHeader, "lblHeader");
			if (headerText != null && headerText.contains(header)) {
				logger.info("Visit header text is matched...");
				return true;
			} else {
				setLastErrorLocator(lblHeader);
				setLastErrorMessage("Failed to match " + header + " header.");
				setLastLogicalErrorMessage("Failed to match " + header + " header!");
				logger.error("TEST STEP ERROR - Failed to match " + header + " header...");
				return false;
			}
		}  else {
			setLastLogicalErrorMessage("Failed to display Visit header!");
			return false;
		}
	}

	public boolean clickOnCloseButton() {
		if(clickOnElement(btnClose, "Close button")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Close button!");
			return false;
		}
	}
	
	public boolean clickOnBeginVisit() {
		if(clickOnElement(btnBeginVisit, "Begin Visit button")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Begin Visit button!");
			return false;
		}
	}

}
