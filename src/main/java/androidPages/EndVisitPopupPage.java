package androidPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import modules.FrameworkConstants;

public class EndVisitPopupPage extends SpruceAppBase {

	private By lblHeader = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/custom']//android.widget.TextView[1]"); 
	
	private By btnNextSteps = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/buttonPanel']//*[@resource-id='android:id/button1']");
	
	private final String PAGE_DISPLAY_NAME = "End Visit Popup";
	
	public EndVisitPopupPage() {
		// if first add is successful, then add the remaining ones. Next time if first one is added, assume that rest would already be added.
		if (addElementDetails(lblHeader.toString(), "End Visit Popup Header", PAGE_DISPLAY_NAME))
		{
			addElementDetails(btnNextSteps.toString(), "Next Steps Button", PAGE_DISPLAY_NAME);
		}
	}
	
	public boolean verifyEndVisitPopupPage(String header){
		addDelay(FrameworkConstants.WAIT_2, "Make sure End Visit Popup page is loaded.");
		WebElement eleHeader = isElementVisible(lblHeader, "lblHeader");
		if (eleHeader!=null) {
			String headerText = getElementText(lblHeader, eleHeader, "lblHeader");
			if (headerText != null && headerText.contains(header)) {
				logger.info("End Visit Popup header text is matched...");
				return true;
			} else {
				setLastErrorLocator(lblHeader);
				setLastErrorMessage("Failed to match " + header + " header.");
				setLastLogicalErrorMessage("Failed to match " + header + " header!");
				logger.error("TEST STEP ERROR - Failed to match " + header + " header...");
				return false;
			}
		}  else {
			setLastLogicalErrorMessage("Failed to display End Visit Popup header!");
			return false;
		}
	}
	
	public boolean clickOnNextStepsButton() {
		if(clickOnElement(btnNextSteps, "Next Steps Button")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Next Steps Button!");
			return false;
		}
	} 
}
