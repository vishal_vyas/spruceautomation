package androidPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import modules.FrameworkConstants;

public class IUnderstandPage extends SpruceAppBase {

	private By btnIUnderstand = By.id("com.spruce.messenger.staging:id/bottomBarButton");
	
	private By lblHeader = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/toolbar']/android.widget.TextView");
	
	private final String PAGE_DISPLAY_NAME = "Understand";
	
	public IUnderstandPage() {
		// if first add is successful, then add the remaining ones. Next time if first one is added, assume that rest would already be added.
		if (addElementDetails(btnIUnderstand.toString(), "Next Button", PAGE_DISPLAY_NAME))
		{
			addElementDetails(lblHeader.toString(), "Understand Header", PAGE_DISPLAY_NAME);
		}
	}
	
	public boolean verifyUnderstandPage(String header){
		addDelay(FrameworkConstants.WAIT_2, "Make sure I Understand page is loaded.");
		WebElement eleHeader = isElementVisible(lblHeader, "lblHeader");
		if (eleHeader!=null) {
			String headerText = getElementText(lblHeader, eleHeader, "lblHeader");
			if (headerText != null && headerText.contains(header)) {
				logger.info("I Understand header text is matched...");
				return true;
			} else {
				setLastErrorLocator(lblHeader);
				setLastErrorMessage("Failed to match " + header + " header.");
				setLastLogicalErrorMessage("Failed to match " + header + " header!");
				logger.error("TEST STEP ERROR - Failed to match " + header + " header...");
				return false;
			}
		}  else {
			setLastLogicalErrorMessage("Failed to display I Understand header!");
			return false;
		}
	}
	
	public boolean clickOnIUnderstandButton() {
		if(clickOnElement(btnIUnderstand, "btnIUnderstand")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on I Understand button!");
			return false;
		}
	} 
}
