package androidPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import modules.FrameworkConstants;

public class YourSymptomsPage extends SpruceAppBase {

	private By lstSymptomsOption = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/compoundButton']"); 
	
	private By selectedSymptomsOption = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/compoundButton' and @checked='true']"); 
	
	private By btnNext = By.id("com.spruce.messenger.staging:id/next");
	
	private By lblHeader = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/toolbar']/android.widget.TextView");
	
	private By btnBack = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/toolbar']/android.widget.ImageButton");
	
	private final String PAGE_DISPLAY_NAME = "Your Symptoms";
	
	public YourSymptomsPage() {
		// if first add is successful, then add the remaining ones. Next time if first one is added, assume that rest would already be added.
		if (addElementDetails(lstSymptomsOption.toString(), "Symptoms Options", PAGE_DISPLAY_NAME))
		{
			addElementDetails(btnNext.toString(), "Next Button", PAGE_DISPLAY_NAME);
			addElementDetails(lblHeader.toString(), "Your Symptoms Header", PAGE_DISPLAY_NAME);
			addElementDetails(btnBack.toString(), "Back button", PAGE_DISPLAY_NAME);
			addElementDetails(selectedSymptomsOption.toString(), "Selected Symptoms", PAGE_DISPLAY_NAME);
		}
	}
	
	public boolean verifyYourSymptomsPage(String header){
		addDelay(FrameworkConstants.WAIT_2, "Make sure Your Symptoms page is loaded.");
		WebElement eleHeader = isElementVisible(lblHeader, "lblHeader");
		if (eleHeader!=null) {
			String headerText = getElementText(lblHeader, eleHeader, "lblHeader");
			if (headerText != null && headerText.contains(header)) {
				logger.info("Your Symptoms header text is matched...");
				return true;
			} else {
				setLastErrorLocator(lblHeader);
				setLastErrorMessage("Failed to match " + header + " header.");
				setLastLogicalErrorMessage("Failed to match " + header + " header!");
				logger.error("TEST STEP ERROR - Failed to match " + header + " header...");
				return false;
			}
		}  else {
			setLastLogicalErrorMessage("Failed to display Your Symptoms header!");
			return false;
		}
	}
	
	public boolean clickOnSymptomsOption(String symptomsOption) {
		WebElement eleSymptom = getSpecificSymptomElement(symptomsOption);
		if(eleSymptom != null) {
			if(clickOnElement(lstSymptomsOption, eleSymptom, "Symptoms Options")) {
				return true;
			} else {
				setLastLogicalErrorMessage("Failed to click on symptom " + symptomsOption + "!");
				return false;
			}
		} else {
			return false;
		}
	}

	private WebElement getSpecificSymptomElement(String symptomsOption) {
		WebElement symptomElement = null;
		List<WebElement> lstSymptoms = getVisibleElementList(lstSymptomsOption, "Symptoms Options");
		if (lstSymptoms!= null && lstSymptoms.size()>0) {
			for (WebElement element : lstSymptoms)	{
				String symptomName = getElementText(lstSymptomsOption, element, "Symptoms Options");
				if (symptomName != null && symptomName.contains(symptomsOption)) {
					symptomElement = element;
					break;
				} else {
					setLastErrorLocator(lstSymptomsOption);
					setLastErrorMessage(symptomsOption +" failed to display!");
					setLastLogicalErrorMessage("Failed to display " + symptomsOption + "!");
				}
			}
		} else {
			setLastErrorLocator(this);
			setLastErrorMessage(this);
			setLastLogicalErrorMessage("Failed to display Symptoms list!");
		}
		return symptomElement;
	}
	
	private WebElement getSpecificSelectedSymptomElement(String symptomsOption) {
		WebElement symptomElement = null;
		List<WebElement> lstSymptoms = getVisibleElementList(selectedSymptomsOption, "Selected Symptoms Options");
		if (lstSymptoms!= null && lstSymptoms.size()>0) {
			for (WebElement element : lstSymptoms)	{
				String symptomName = getElementText(lstSymptomsOption, element, "Selected Symptoms Options");
				if (symptomName != null && symptomName.contains(symptomsOption)) {
					symptomElement = element;
					break;
				} else {
					setLastErrorLocator(lstSymptomsOption);
					setLastErrorMessage(symptomsOption +" failed to display!");
					setLastLogicalErrorMessage("Failed to display " + symptomsOption + "!");
				}
			}
		} else {
			setLastErrorLocator(this);
			setLastErrorMessage(this);
			setLastLogicalErrorMessage("Failed to display Selected Symptoms list!");
		}
		return symptomElement;
	}
	
	public boolean clickOnNextButton() {
		if(clickOnElement(btnNext, "Next Button")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Next Button!");
			return false;
		}
	} 

	public boolean clickOnBackButton() {
		if(clickOnElement(btnBack, "Back Button")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Back Button!");
			return false;
		}
	} 

	public boolean verifySymptomSelected(String symptomsOption) {
		WebElement eleSymptom = getSpecificSelectedSymptomElement(symptomsOption);
		if(eleSymptom != null) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to Save visit with symptom " + symptomsOption + "!");
			return false;
		}
	}

}
