package androidPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import modules.FrameworkConstants;

public class LoginPage extends SpruceAppBase  {

	private By txtEmailLocator = By.id("com.spruce.messenger.staging:id/email");
	
	private By txtPasswordLocator = By.id("com.spruce.messenger.staging:id/password");
	
	private By btnloginLocator = By.id("com.spruce.messenger.staging:id/signIn");

	private By btnCreateAccountOnLoginLocator = By.id("com.reddit.frontpage:id/register_prompt");
	
	private By btnForgotPasswordLocator = By.id("com.spruce.messenger.staging:id/resetPassword");
		
	private final String PAGE_DISPLAY_NAME = "Login";
	
	public LoginPage() {
		// if first add is successful, then add the remaining ones. Next time if first one is added, assume that rest would already be added.
		if (addElementDetails(btnForgotPasswordLocator.toString(), "Forgot Password button", PAGE_DISPLAY_NAME))
		{
			addElementDetails(txtPasswordLocator.toString(), "Password textbox", PAGE_DISPLAY_NAME);
			addElementDetails(btnloginLocator.toString(), "Login button", PAGE_DISPLAY_NAME);
			addElementDetails(txtEmailLocator.toString(), "Email textbox", PAGE_DISPLAY_NAME);
			addElementDetails(btnCreateAccountOnLoginLocator.toString(), "Create Account button", PAGE_DISPLAY_NAME);
		}
	}
	
	public boolean verifyLoginPage() {
		addDelay(FrameworkConstants.WAIT_2, "Make sure Login page is loaded successfully.");
		WebElement txtEmail = isElementVisible(txtEmailLocator, "Email textbox");
		if (txtEmail==null)
			setLastLogicalErrorMessage("Failed to verify Login page!");
		return txtEmail!=null;
	}
	
	public boolean enterEmail(String email) {
		if (addTextInTextBox(txtEmailLocator, "txtEmail", email)) {
			pressDeviceBackButton();
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to add email " + email + "!");
			return false;
		}
	}
	
	public boolean enterPassword(String password) {
		if (addTextInTextBox(txtPasswordLocator, "txtPassword", password)) {
			pressDeviceBackButton();
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to add password " + password + "!");
			return false;
		}
	}
	
	public boolean clickOnLoginButton() {
		if(clickOnElement(btnloginLocator, "btnlogin")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Login button!");
			return false;
		}
	}
	
	public boolean clickOnSignupButton() {
		if(clickOnElement(btnCreateAccountOnLoginLocator, "btnCreateAccountOnLogin")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Create Account button!");
			return false;
		}
	}
	
	public boolean clickOnForgotPasswordButton() {
		if(clickOnElement(btnForgotPasswordLocator, "btnForgotPassword")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Forgot Password button!");
			return false;
		}
	}
}
