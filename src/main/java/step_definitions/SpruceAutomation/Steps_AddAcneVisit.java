package step_definitions.SpruceAutomation;

import androidPages.EndVisitPopupPage;
import androidPages.HomePage;
import androidPages.IUnderstandPage;
import androidPages.StartVisitPage;
import androidPages.VisitAlertPopupPage;
import androidPages.VisitPage;
import androidPages.YourSymptomsPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps_AddAcneVisit extends Steps_Base {

	HomePage homePage = new HomePage();
	VisitAlertPopupPage visitAlertPopupPage = new VisitAlertPopupPage();
	StartVisitPage startVisitPage = new StartVisitPage();
	VisitPage visitPage = new VisitPage();
	YourSymptomsPage yourSymptomsPage = new YourSymptomsPage();
	EndVisitPopupPage endVisitPopupPage = new EndVisitPopupPage();
	IUnderstandPage iUnderstandPage = new IUnderstandPage();
	
	@Given("^\"([^\"]*)\" I am on Home page$")
	public void i_am_on_home_page(String arg1) {
		abortScenarioIfFalse(homePage, homePage.verifyHomePage(arg1));
    }
	
	@When("^I tap on Send a Message textbox$")
	public void i_tap_on_Send_a_Message_textbox() {
		abortScenarioIfFalse(homePage, homePage.clickOnSendMessageTextBox());
	}

	@Then("^Sending options are shown$")
	public void sending_options_are_shown() {
		abortScenarioIfFalse(homePage, homePage.verifyComposeMessageOption());
	}

	@When("^I tap on Start Visit option$")
	public void i_tap_on_Start_Visit_option() {
		abortScenarioIfFalse(homePage, homePage.clickOnStartVisitOption());
	}

	@Then("^I redirected to \"([^\"]*)\" page$")
	public void i_redirected_to_page(String arg1) {
		boolean isVisitAlertShown = visitAlertPopupPage.verifyVisitAlertPopupPage();
		if (isVisitAlertShown) {
			abortScenarioIfFalse(visitAlertPopupPage,  visitAlertPopupPage.clickOnDeleteDiscardVisitButton());
			abortScenarioIfFalse(visitAlertPopupPage,  visitAlertPopupPage.confirmDeleteVisit());
		}
		abortScenarioIfFalse(startVisitPage, startVisitPage.verifyStartVisitPage(arg1));
	}

	@Then("^List of Visits are shown$")
	public void list_of_visits_shown() {
		abortScenarioIfFalse(startVisitPage, startVisitPage.verifyVisitOption());
	}

	@When("^I tap on \"([^\"]*)\" visit$")
	public void i_tap_on_visit(String arg1) {
		abortScenarioIfFalse(startVisitPage, startVisitPage.clickOnVisitOption(arg1));
	}

	@Then("^\"([^\"]*)\" visit begin page is shown$")
	public void visit_begin_page_is_shown(String arg1) {
		abortScenarioIfFalse(visitPage, visitPage.verifyVisitPage(arg1));
	}

	@When("^I tap on Begin button$")
	public void i_tap_on_Begin_button() {
		abortScenarioIfFalse(visitPage, visitPage.clickOnBeginVisit());
	}

	@Then("^\"([^\"]*)\" page is opened$")
	public void page_is_opened(String arg1) {
		abortScenarioIfFalse(yourSymptomsPage, yourSymptomsPage.verifyYourSymptomsPage(arg1));
	}

	@When("^I select \"([^\"]*)\" symptom$")
	public void i_select_symptoms(String arg1) {
		abortScenarioIfFalse(yourSymptomsPage, yourSymptomsPage.clickOnSymptomsOption(arg1));
	}

	@When("^I tap on next button$")
	public void i_tap_on_next_button() {
		abortScenarioIfFalse(yourSymptomsPage, yourSymptomsPage.clickOnNextButton());
	}

	@Then("^End Visit popup is shown with title \"([^\"]*)\"$")
	public void end_Visit_popup_is_shown(String arg1) {
		abortScenarioIfFalse(endVisitPopupPage, endVisitPopupPage.verifyEndVisitPopupPage(arg1));
	}

	@When("^I tap on next steps button$")
	public void i_tap_on_next_steps_button() {
		abortScenarioIfFalse(endVisitPopupPage, endVisitPopupPage.clickOnNextStepsButton());
	}

	@Then("^\"([^\"]*)\" page is shown with I Understand button$")
	public void page_is_shown_with_I_Understand_button(String arg1) {
		abortScenarioIfFalse(iUnderstandPage, iUnderstandPage.verifyUnderstandPage(arg1));
	}

	@When("^I tap on I Understand button$")
	public void i_tap_on_I_Understand_button() {
		abortScenarioIfFalse(iUnderstandPage, iUnderstandPage.clickOnIUnderstandButton());
	}

	@Then("^I redirected to Home page$")
	public void i_redirected_to_Home_page() {
		abortScenarioIfFalse(homePage, homePage.verifyHomePageAfterCreatingVisit()); 
	}

	@Then("^\"([^\"]*)\" visit is shown current timestamp$")
	public void visit_is_shown_current_timestamp(String arg1) {
		abortScenarioIfFalse(homePage, homePage.verifyVisitTitle(arg1)); 
		abortScenarioIfFalse(homePage, homePage.verifyVisitTimestamp()); 
	}
}
