package androidPages;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import modules.FrameworkConstants;

public class HomePage extends SpruceAppBase {

	private By txtSendMessage = By.id("com.spruce.messenger.staging:id/reply");

	private By btnLeftNav = By.id("com.spruce.messenger.staging:id/openDrawer");
	
	private By txtComposeMessage = By.id("com.spruce.messenger.staging:id/text");
	
	private By lstComposeMessageOption = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/composerMenu']/android.widget.ImageView"); 
	
	private By btnSend = By.id("com.spruce.messenger.staging:id/send");
	
	private By lstVisitTimeStamp = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/list']//*[@resource-id='com.spruce.messenger.staging:id/timestamp']");
	
	private By lstCreatedVisitTitle = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/list']//*[@resource-id='com.spruce.messenger.staging:id/subtitle']");
	
	private final String PAGE_DISPLAY_NAME = "Home";
	
	public HomePage() {
		// if first add is successful, then add the remaining ones. Next time if first one is added, assume that rest would already be added.
		if (addElementDetails(txtSendMessage.toString(), "Send Message Textbox", PAGE_DISPLAY_NAME))
		{
			addElementDetails(btnLeftNav.toString(), "Left Nav button", PAGE_DISPLAY_NAME);
			addElementDetails(txtComposeMessage.toString(), "Compose Message Textboox", PAGE_DISPLAY_NAME);
			addElementDetails(lstComposeMessageOption.toString(), "Compose Message Options", PAGE_DISPLAY_NAME);
			addElementDetails(btnSend.toString(), "Send button", PAGE_DISPLAY_NAME);
			addElementDetails(lstCreatedVisitTitle.toString(), "Created Visit Title List", PAGE_DISPLAY_NAME);
			addElementDetails(lstVisitTimeStamp.toString(), "Visit TimeStamp List", PAGE_DISPLAY_NAME);
		}
	}
	
	public boolean verifyHomePage(String target){
		addDelay(FrameworkConstants.WAIT_3, "Make sure Home page is loaded.");
		WebElement eleSendMessage = isElementVisible(target, txtSendMessage, "txtSendMessage");
		WebElement eleLeftNav = isElementVisible(target, btnLeftNav, "btnLeftNav");
		if (eleSendMessage==null || eleLeftNav==null) {
			setLastLogicalErrorMessage("Failed to verify Home page!");
			return false;
		} else {
			addDelay(FrameworkConstants.WAIT_5, "Make sure Video is recording...");
			return true;
		}
	}
	
	public boolean verifyHomePageAfterCreatingVisit(){
		addDelay(FrameworkConstants.WAIT_3, "Make sure Home page is loaded.");
		WebElement eleSendMessage = isElementVisible(txtComposeMessage, "Compose Message Options");
		WebElement eleLeftNav = isElementVisible(btnLeftNav, "btnLeftNav");
		if (eleSendMessage==null || eleLeftNav==null) {
			setLastLogicalErrorMessage("Failed to verify Home page after creating visit!");
			return false;
		} else {
			return true;
		}
	}
	
	public boolean clickOnSendMessageTextBox() {
		if(clickOnElement(txtSendMessage, "txtSendMessage")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Send Message textbox!");
			return false;
		}
	}
	
	public boolean verifyComposeMessageOption() {
		List<WebElement> lstComposeMessage = getVisibleElementList(lstComposeMessageOption, "Compose Message Options");
		if (lstComposeMessage != null && lstComposeMessage.size()>0) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to display List of Compose Message options!");
			return false;
		}
	}
	
	public boolean clickOnStartVisitOption() {
		if(clickOnElementIfVisible(lstComposeMessageOption, "Compose Message Options", 1)) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Start Visit option!");
			return false;
		}
	}

	public boolean verifyVisitTitle(String title) {
		List<WebElement> lstVisitTitle = getVisibleElementList(lstCreatedVisitTitle, "Visit Title list");
		if (lstVisitTitle!= null && lstVisitTitle.size()>0) {
			String visitTitle = getElementTextUsingIndex(lstCreatedVisitTitle, lstVisitTitle, "Visit Title list", lstVisitTitle.size()-1);
			if(visitTitle != null && visitTitle.contains(title)) {
				return true;
			} else {
				setLastLogicalErrorMessage("Failed to display created Visit "+title+"!");
				return false;
			}
		} else {
			setLastErrorLocator(this);
			setLastErrorMessage(this);
			setLastLogicalErrorMessage("Failed to display Visit Title list!");
			return false;
		}
	}
	
	public boolean verifyVisitTimestamp() {
		String currentTime = getCurrentTime();
		String visitTime = getVisitTime();
		logger.info("Machine Current Time: " + currentTime);
		logger.info("Created Visit Time: " + visitTime);
		if (visitTime != null && currentTime != null && visitTime.equals(currentTime)) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to match created Visit timestamp!");
			return false;
		}
	}
	
	private String getVisitTime() {
		String visitTimestamp = null;
		List<WebElement> lstVisitTimestamp = getVisibleElementList(lstVisitTimeStamp, "Visit Timestamp list");
		if (lstVisitTimestamp!= null && lstVisitTimestamp.size()>0) {
			visitTimestamp = getElementTextUsingIndex(lstVisitTimeStamp, lstVisitTimestamp, "Visit Timestamp list", lstVisitTimestamp.size()-1);
		} else {
			setLastErrorLocator(this);
			setLastErrorMessage(this);
			setLastLogicalErrorMessage("Failed to display Visit Timestamp list!");
		}
		return visitTimestamp;
	}
	
	private String getCurrentTime() {
		LocalTime localTime = LocalTime.now();
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("h:mm a");
		String currentTime = localTime.format(dateTimeFormatter);
		return currentTime;
	}
}
