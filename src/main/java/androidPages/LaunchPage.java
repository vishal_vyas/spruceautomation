package androidPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import modules.FrameworkConstants;

public class LaunchPage extends SpruceAppBase {
	
	private By lblWelcomeLocator = By.id("com.spruce.messenger.staging:id/title");

	private By btnloginOnLaunchLocator = By.id("com.spruce.messenger.staging:id/login");
	
	private By btnCreateAccountOnLaunchLocator = By.id("com.spruce.messenger.staging:id/createAccount");
	
	private final String PAGE_DISPLAY_NAME = "Launch";
	
	public LaunchPage() {
		// if first add is successful, then add the remaining ones. Next time if first one is added, assume that rest would already be added.
		if (addElementDetails(lblWelcomeLocator.toString(), "Welcome label", PAGE_DISPLAY_NAME))
		{
			addElementDetails(btnCreateAccountOnLaunchLocator.toString(), "Create Account button", PAGE_DISPLAY_NAME);
			addElementDetails(btnloginOnLaunchLocator.toString(), "Login button", PAGE_DISPLAY_NAME);
		}
	}
	
	public boolean verifyLaunchPage(){
		addDelay(FrameworkConstants.WAIT_2, "Make sure Launch page is loaded.");
		WebElement lblWelcomeNote = isElementVisible(lblWelcomeLocator, "Welcome label");
		if (lblWelcomeNote==null) 
			setLastLogicalErrorMessage("Failed to verify Launch page!");
		return lblWelcomeNote!=null;
	}
	
	public boolean clickOnLoginButton() {
		if (clickOnElement(btnloginOnLaunchLocator, "btnloginOnLaunch")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Login button!");
			return false;
		}
	}
	
	public boolean clickOnSignupButton() {
		if (clickOnElement(btnCreateAccountOnLaunchLocator, "btnCreateAccountOnLaunch")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Create Account button!");
			return false;
		}
	}

}
