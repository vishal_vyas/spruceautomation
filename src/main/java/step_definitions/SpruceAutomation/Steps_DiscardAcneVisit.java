package step_definitions.SpruceAutomation;

import androidPages.VisitAlertPopupPage;
import androidPages.VisitPage;
import androidPages.YourSymptomsPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps_DiscardAcneVisit extends Steps_Base {

	VisitAlertPopupPage visitAlertPopupPage = new VisitAlertPopupPage();
	VisitPage visitPage = new VisitPage();
	YourSymptomsPage yourSymptomsPage = new YourSymptomsPage();
	
	@When("^I tap on back button from Symptoms page$")
	public void i_tap_on_back_button() {
		abortScenarioIfFalse(yourSymptomsPage, yourSymptomsPage.clickOnBackButton());
	}

	@Then("^I redirected to \"([^\"]*)\" visit begin page$")
	public void i_redirected_to_visit_begin_page(String arg1) {
		abortScenarioIfFalse(visitPage, visitPage.verifyVisitPage(arg1));
	}

	@When("^I tap on close button from Begin Visit page$")
	public void i_tap_on_close_button() {
		abortScenarioIfFalse(visitPage, visitPage.clickOnCloseButton());
	}

	@Then("^Verify Save draft or discard popup is shown$")
	public void verify_Save_draft_or_discard_popup_is_shown() {
		abortScenarioIfFalse(visitAlertPopupPage, visitAlertPopupPage.verifyVisitAlertPopupPage());
	}

	@When("^I tap on discard button$")
	public void i_tap_on_discard_button() {
		abortScenarioIfFalse(visitAlertPopupPage, visitAlertPopupPage.clickOnDeleteDiscardVisitButton());
	}
}
