package androidPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import modules.FrameworkConstants;

public class VisitAlertPopupPage extends SpruceAppBase {

	private By lblVisitAlert = By.id("com.spruce.messenger.staging:id/alertTitle");
	
	private By btnContinueSaveVisit = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/buttonPanel']//*[@resource-id='android:id/button1']");
	
	private By btnDeleteDiscardVisit = By.xpath("//*[@resource-id='com.spruce.messenger.staging:id/buttonPanel']//*[@resource-id='android:id/button2']");
	
	private By lblVisitDeleteConfirmation = By.id("android:id/message");
	
	private final String PAGE_DISPLAY_NAME = "Visit Alert Popup";
	
	public VisitAlertPopupPage() {
		// if first add is successful, then add the remaining ones. Next time if first one is added, assume that rest would already be added.
		if (addElementDetails(lblVisitAlert.toString(), "Visit Alert Header", PAGE_DISPLAY_NAME))
		{
			addElementDetails(btnContinueSaveVisit.toString(), "Continue/Save Visit Button", PAGE_DISPLAY_NAME);
			addElementDetails(btnDeleteDiscardVisit.toString(), "Delete/Discard Visit Button", PAGE_DISPLAY_NAME);
			addElementDetails(lblVisitDeleteConfirmation.toString(), "Visit Delete Confirmation", PAGE_DISPLAY_NAME);
		}
	}
	
	public boolean verifyVisitAlertPopupPage(){
		addDelay(FrameworkConstants.WAIT_2, "Make sure Visit Alert Popup page is loaded.");
		WebElement eleAlert = isElementVisible(lblVisitAlert, "lblVisitAlert");
		if (eleAlert!=null) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean clickOnDeleteDiscardVisitButton() {
		if(clickOnElement(btnDeleteDiscardVisit, "Delete/Discard Visit Button")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Delete/Discard Visit Button!");
			return false;
		}
	} 
	
	public boolean clickOnContinueSaveVisitButton() {
		if(clickOnElement(btnContinueSaveVisit, "Continue/Save Visit Button")) {
			return true;
		} else {
			setLastLogicalErrorMessage("Failed to click on Continue/Save Visit Button!");
			return false;
		}
	} 
	
	public boolean confirmDeleteVisit() {
		WebElement eleDeleteAlertConfirmation = isElementVisible(lblVisitDeleteConfirmation, "lblVisitDeleteConfirmation");
		if (eleDeleteAlertConfirmation!=null) {
			if(clickOnElement(btnContinueSaveVisit, "Delete Visit Button")) {
				return true;
			} else {
				setLastLogicalErrorMessage("Failed to click on Delete Visit Button!");
				return false;
			}
		} else {
			setLastErrorLocator(lblVisitDeleteConfirmation);
			setLastErrorMessage("Failed to display Delete Visit Confirmation Alert!");
			setLastLogicalErrorMessage("Failed to display Delete Visit Confirmation Alert!");
			return false;
		}
	}
}
