

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;

import modules.FrameworkConstants;


@RunWith(ExtendedCucumber.class)

public class RunCukesTest {
	
	
	
	@BeforeClass
	// SETUP THE TEST ENVIRONMENT
	// CALLS HOOK ONCE ENVIRONMENT IS SET
	public static void setUp() {
		Logger logger = Logger.getLogger(FrameworkConstants.TESTRUN_LOGGER);
		logger.info("");
		logger.info("*** STARTING AUTOMATION SUITE. ***");

	}

	@AfterClass
	// CALL ON THE END OF TEST CASE
	// QUITING DRIVER
	public static void tearDown() throws IOException {
		Logger logger = Logger.getLogger(FrameworkConstants.TESTRUN_LOGGER);
		logger.info("*** AUTOMATION SUITE COMPLETED. **** ");
		logger.info("");
	}
}
