Feature: spruce-visit

	@DevDone @spruce-visit @add-acne-visit
    Scenario: add-acne-visit
    Given I am on Home page 
    When I tap on Send a Message textbox
    Then Sending options are shown
    When I tap on Start Visit option
    Then I redirected to "Start a Visit" page
    And List of Visits are shown
    When I tap on "Acne" visit
    Then "Acne" visit begin page is shown
    When I tap on Begin button
    Then "Your Symptoms" page is opened
    When I select "Fever or shaking chills" symptom
    And I tap on next button
    Then End Visit popup is shown with title "We're going to have to end your visit here."
    When I tap on next steps button
   	Then "Next Steps" page is shown with I Understand button
   	When I tap on I Understand button
   	Then I redirected to Home page 
   	And "Acne" visit is shown current timestamp
   	
   	@DevDone @spruce-visit @discard-acne-visit
    Scenario: discard-acne-visit
    Given I am on Home page 
    When I tap on Send a Message textbox
    Then Sending options are shown
    When I tap on Start Visit option
    Then I redirected to "Start a Visit" page
    And List of Visits are shown
    When I tap on "Acne" visit
    Then "Acne" visit begin page is shown
    When I tap on Begin button
    Then "Your Symptoms" page is opened
    When I select "Fever or shaking chills" symptom
    And I tap on back button from Symptoms page
    Then I redirected to "Acne" visit begin page
    When I tap on close button from Begin Visit page
   	Then Verify Save draft or discard popup is shown
   	When I tap on discard button
   	Then I redirected to Home page
   	
   	@DevDone @spruce-visit @save-acne-visit
    Scenario: save-acne-visit
    Given I am on Home page 
    When I tap on Send a Message textbox
    Then Sending options are shown
    When I tap on Start Visit option
    Then I redirected to "Start a Visit" page
    And List of Visits are shown
    When I tap on "Acne" visit
    Then "Acne" visit begin page is shown
    When I tap on Begin button
    Then "Your Symptoms" page is opened
    When I select "Fever or shaking chills" symptom
    And I tap on back button from Symptoms page
    Then I redirected to "Acne" visit begin page
    When I tap on close button from Begin Visit page
   	Then Verify Save draft or discard popup is shown
   	When I tap on Save Draft button
   	Then I redirected to Home page
   	When I tap on Start Visit option
    Then Verify Continue Visit popup is shown
    When I tap on Continue Visit button
    Then "Acne" visit begin page is shown
    When I tap on Begin button
    Then Verify "Fever or shaking chills" symptom is shown selected
    